<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Models\User; 
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function store(Request $request){
        $field = $request->validate([
            'name'=> 'required|string', 
            'email'=> 'required|string|unique:users,email', 
            'password'=> 'required|string'
        ]);
        $user = User::create([
            'name' => $field['name'],
            'email' => $field['email'],
            'password' => bcrypt($field['password']),
            'api_token' => Str::random(60) 
            ]
        ); 
     
        return response($user, 201); 
    }
    public function login(Request $request){
        $field = $request->validate([
            'email'=> 'required|string|', 
            'password'=> 'required|string|'
        ]);

        if(Auth::attempt($request->only('email', 'password'))){
            return response(Auth::user(), 200); 
        }
        else {
            return response("Wrong login credential"); 
        }
    }
}
