<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Todo; 
use App\Models\User; 
use Illuminate\Support\Facades\Auth; 

class TaskController extends Controller
{
    public function store(Request $request){
        $task = new Todo([
            'title' => $request->title,
            'user_id' => Auth::id(), 
            'status'=> $request->status
        ]); 
        $task->save(); 
        return response($task, 200);
    }

    public function edit(Request $request, $TaskId){
        $task = Todo::findOrFail($TaskId); 
        $task->fill($request->input())->save(); 
        return response($task,201); 
    }

    public function delete(Request $request, $TaskId){
        $task = Todo::findOrFail($TaskId)->delete(); 
        return response("success",202);
    }

    public function getTodoByUser(Request $request, $user_id){
        $todo = User::findOrFail($user_id)->todo; 
        return response($todo,200); 
    }
    public function getAllPublic(Request $request){
        $todo = Todo::where('status', 'public')->get(); 
        return response($todo); 
    }
}
