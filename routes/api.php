<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TaskController;


Route::post('/signup',[UserController::class, 'store']); 
Route::post('/login',[UserController::class, 'login']); 

Route::get('/todo', [TaskController::class, 'getAllPublic']);  


Route::group(['prefix' => 'todo', 'middleware' => 'auth:api'], function()
{
    Route::put('/{task_id}', [TaskController::class, 'edit']);
    Route::post('/', [TaskController::class, 'store']); 
    Route::get('/{user_id}', [TaskController::class, 'getTodoByUser']);  
    Route::delete('/{task_id}', [TaskController::class, 'delete']); 
});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
